# Project Update

Group Project Milestone #2

Due: 11/15/21

Each group will submit a project update, roughly one page in length, describing the work they have completed so far on their analysis. This report should include preliminary results for the team's analysis and list any roadblocks the team has identified during their initial work. This will give the instructors an opportunity to learn about each team’s work and help resolve any issues identified by the team. This milestone is worth 15/100 points.
